const
	PROXY_TYPE_HTTP = 1,
	PROXY_TYPE_HTTPS = 2,
	PROXY_ANONYMITY_ANM = 1,
	PROXY_ANONYMITY_HIA = 2,
	PROXY_ANONYMITY_NOA = 3;

var
	request = require('request'),
	jsdom = require('jsdom'),
	moment = require('moment'),
	url = 'http://spys.ru/free-proxy-list/ALL/';	

// console.log('Parsing proxies...');
// console.log('Downloading page contents (' + url + ')...');
request.post(url, {xpp: 3}, function (err, response, body) {
	
	// Check for error
	if (err) {
		console.error('Failed to download page contents, reason: ' + err);
		return;
	}

	// Check http status code
	if (response.statusCode != 200) {
		console.error('Failed to download page contents, bad http status code: ' + response.statusCode);
		return;
	}

	// Parse page contents
	// console.log('Download success! Parsing contents...');
	jsdom.env({
		html: body,
		scripts: ['./node_modules/jquery/dist/jquery.min.js'],
		done: function (err, window) {

			if (err) {
				console.error('Failed to parse page contents, reason: ' + err);
				return;
			}

			var rows = window.$('tr.spy1x, tr.spy1xx');
			if (rows.length < 3) {
				console.error('Failed to parse page contents, invalid rows count');
				return;
			}

			var data = {
				proxies: [],
				errors: []
			};

			rows = rows.slice(2);
			rows.each(function (index) {
				
				var row = window.$(this);
				var proxy = {};

				var cells = row.find('td');

				try {

					getAddressAndPort(proxy, window.$(cells[0]), window);
					getTypeAndSoftware(proxy, window.$(cells[1]));
					getAnonymity(proxy, window.$(cells[2]));

					var countryAndCity = getCountryAndCity(window.$(cells[3]));
					proxy.country = countryAndCity.country;
					proxy.city = countryAndCity.city;

					proxy.hostname = window.$(cells[4]).text().trim();
					proxy.latency = window.$(cells[5]).text().trim();

					getUptime(proxy, window.$(cells[8]));
					getCheckedAt(proxy, window.$(cells[9]));

					data.proxies.push(proxy);

				} catch (e) {
					data.errors.push(e.message);
				}

			});

			console.log(JSON.stringify(data));

			// Free memory associated with the window
			window.close();
		}
	});
});

/**
 * Saves a proxy
 *
 * @param countryAndCity {Object}
 * @param proxy {Object}
 */
function saveProxy(countryAndCity, proxy) {

}

/**
 * Gets proxy address and port
 *
 * @param proxy {Object}
 * @param cell {jQuery}
 */
function getAddressAndPort(proxy, cell, window) {

	var cellContent = cell.text();
	var pattern = /([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/;
	var matches = pattern.exec(cellContent);
	proxy.address = matches[1];

	var tables = window.$('table');
	var firstTable = window.$(tables[0]);
	tables = null;
	var portValuesDecoderScript = firstTable.next().text().trim();
	eval(portValuesDecoderScript);

	var portScript = cell.find('script').text();
	pattern = /\+(\(.+\))\)/;
	matches = pattern.exec(portScript);
	portScript = matches[1].replace('+', ' + \'\' + ');
	pattern = matches = null;
	var portValue = parseInt(eval(portScript));
	proxy.port = portValue;
}

/**
 * Gets type and software
 *
 * @param proxy {Object}
 * @param cell {jQuery}
 */
function getTypeAndSoftware(proxy, cell) {
	var typeAsString = cell.find('font.spy1').text().trim();
	switch (typeAsString) {
		case 'HTTP':
			proxy.type = PROXY_TYPE_HTTP;
			break;
		case 'HTTPS':
			proxy.type = PROXY_TYPE_HTTPS;
			break;
		default:
			throw new Error('Unknown proxy type: ' + typeAsString);		
	}
	typeAsString = null;

	var software = cell.find('font.spy5').text().trim();
	if (software.length == 0) {
		proxy.software = null;
		return;
	}
	proxy.software = software.replace('(', '').replace(')', '');
}

/**
 * Gets anonymity
 *
 * @param proxy {Object}
 * @param cell {jQuery}
 */
function getAnonymity(proxy, cell) {
	var anonymityAsString = cell.text().trim();
	switch (anonymityAsString) {
		case 'ANM':
			proxy.anonymity = PROXY_ANONYMITY_ANM;
			break;
		case 'HIA':
			proxy.anonymity = PROXY_ANONYMITY_HIA;
			break;
		case 'NOA':
			proxy.anonymity = PROXY_ANONYMITY_NOA;
			break;
		default:
			throw new Error('Unknown anonymity level: ' + anonymityAsString);			
	}
}

/**
 * Gets country and city
 *
 * @param cell {jQuery}
 * @return {Object}
 */
function getCountryAndCity(cell) {
	var result = {};
	var parts = cell.text().trim().split('(');
	result.country = parts[0].trim().replace(' !!!', '');
	result.city = parts.length > 1 ? parts[1].replace(')', '') : null;
	return result;
}

/**
 * Gets uptime (in percents)
 *
 * @param proxy {Object}
 * @param cell {jQuery}
 */
function getUptime(proxy, cell) {
	var pattern = /([0-9]+)\%/;
	var cellContent = cell.text().trim();
	var matches = pattern.exec(cellContent);
	if (!matches) {
		proxy.uptime = 0;
		return;
	}
	proxy.uptime = parseInt(matches[1]);
}

/**
 * Gets checked at date
 *
 * @param proxy {Object}
 * @param cell {jQuery}
 */
function getCheckedAt(proxy, cell) {
	var datetimeString = cell.find('font.spy1').text().trim();
	var dt = Date.parse(datetimeString);
	var mDt = moment(dt);
	proxy.checked_at = mDt.format('YYYY-MM-DD HH:mm:ss');
}